﻿'use strict';

var gulp = require('gulp'),
    vinylPaths = require('vinyl-paths'),
    del = require('del'),
    rename = require('gulp-rename'),
    minimist = require('minimist'),
    revReplace = require('gulp-rev-replace'),
    RevAll = require('gulp-rev-all'),
    htmlReplace = require('gulp-html-replace');

// This enables passing a gulp parameter to detect the
// environment from MS Build (see Gulp-Rename-Config task in the gulp.targets file)
var knownOptions = {
    string: 'env',
    default: { env: process.env.NODE_ENV || 'LOCAL' }
};

var buildTeamEnvironments = ['FULL01', 'FULL02', 'FULL03', 'FULL04', 'PROD', 'SUPPORT', 'THIN01', 'THIN02', 'THIN03', 'THIN04', 'THIN05',
                            'THIN06', 'THIN07', 'THIN08', 'THIN09', 'THIN10', 'THIN11', 'THIN12', 'THIN13', 'THIN14'];

// This parses the gulp parameter
var options = minimist(process.argv.slice(2), knownOptions);

// This deletes all of the files in the dist folder
gulp.task('clean-up', function () {
    return gulp.src([
        'dist/app', 'dist/content', 'dist/index', 'dist/scripts', 'dist/rev-manifest.json'])
        .pipe(vinylPaths(del));
});

// This performs a "config.json transform"
gulp.task('rename-config', function () {
    return gulp.src('app/config.' + options.env.toUpperCase() + '.json')
        .pipe(rename('config.json'))
        .pipe(gulp.dest('app'));
});

// This copies all fonts to the dist folder
gulp.task('copy-fonts', function() {
    return gulp.src('content/fonts/**/*.*')
        .pipe(gulp.dest('dist/content/fonts'));
});

// This copies all images to the dist folder
gulp.task('copy-images', function() {
    return gulp.src('content/img/**/*.*')
        .pipe(gulp.dest('dist/content/img'));
});

// This renames all content files (css, JavaScript) for cache-busting
// and creates a manifest that can be used in the "replace-index" task below
gulp.task('version-resources', function () {

    var revAll = new RevAll();

    return gulp.src(['app/**/*.js', 'content/css/**/*.css', 'scripts/**/*.js'])
        .pipe(revAll.revision())
        .pipe(gulp.dest('dist'))
        .pipe(revAll.manifestFile())
        .pipe(gulp.dest('dist'));
});

// These tasks replace the css and JavaScript references in each index page

// Login
gulp.task('replace-index-login', function () {
    var manifest = gulp.src('dist/rev-manifest.json');

    return gulp.src('index.html')
        .pipe(revReplace({ manifest: manifest, prefix: "dist/" }))
        .pipe(gulp.dest('dist/index/login'));
});

// Binder
gulp.task('replace-index-binder', function () {
    var manifest = gulp.src('dist/rev-manifest.json');

    return gulp.src('app/smallBusiness/binder/index.html')
        .pipe(revReplace({ manifest: manifest, prefix: "dist/" }))
        .pipe(gulp.dest('dist/index/binder'));
});

// Quote
gulp.task('replace-index-quote', function () {
    var manifest = gulp.src('dist/rev-manifest.json');

    return gulp.src('app/smallBusiness/quote/index.html')
        .pipe(revReplace({ manifest: manifest, prefix: "dist/" }))
        .pipe(gulp.dest('dist/index/quote'));
});

// Endorsement
gulp.task('replace-index-endorsement', function () {
    var manifest = gulp.src('dist/rev-manifest.json');

    return gulp.src('app/smallBusiness/endorsement/index.html')
        .pipe(revReplace({ manifest: manifest, prefix: "dist/" }))
        .pipe(gulp.dest('dist/index/endorsement'));
});

// These tasks replace the element <base href="/" /> with <base href="/spa/" /> for UAT++ environments in the html files
gulp.task('replace-html-base-login', function () {
    if (buildTeamEnvironments.indexOf(options.env.toUpperCase()) !== -1) {
        return gulp.src('dist/index/login/index.html')
            .pipe(htmlReplace({
                spa: {
                    src: null,
                    tpl: '<base href="/spa/" />'
                }
            }))
            .pipe(gulp.dest('dist/index/login'));
    }
});
gulp.task('replace-html-base-binder', function () {
    if (buildTeamEnvironments.indexOf(options.env.toUpperCase()) !== -1) {
        return gulp.src('dist/index/binder/index.html')
            .pipe(htmlReplace({
                spa: {
                    src: null,
                    tpl: '<base href="/spa/" />'
                }
            }))
            .pipe(gulp.dest('dist/index/binder'));
    }
});
gulp.task('replace-html-base-quote', function () {
    if (buildTeamEnvironments.indexOf(options.env.toUpperCase()) !== -1) {
        return gulp.src('dist/index/quote/index.html')
            .pipe(htmlReplace({
                spa: {
                    src: null,
                    tpl: '<base href="/spa/" />'
                }
            }))
            .pipe(gulp.dest('dist/index/quote'));
    }
});
gulp.task('replace-html-base-endorsement', function () {
    if (buildTeamEnvironments.indexOf(options.env.toUpperCase()) !== -1) {
        return gulp.src('dist/index/endorsement/index.html')
            .pipe(htmlReplace({
                spa: {
                    src: null,
                    tpl: '<base href="/spa/" />'
                }
            }))
            .pipe(gulp.dest('dist/index/endorsement'));
    }
});