﻿/// <binding BeforeBuild='css' />
'use strict';

require('es6-promise').polyfill();

var gulp = require('gulp');
var autoprefixer = require('autoprefixer');
var babelify = require('babelify');
var browserify = require('gulp-browserify');
var concat = require('gulp-concat');
var cssmin = require('gulp-cssmin');
var postcss = require('gulp-postcss');
var rename = require('gulp-rename');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');

var postCssProcessors = [
        autoprefixer({ browsers: ['last 20 versions'] })
];

gulp.task('css', function () {
    gulp.src('src/**/*.css')
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss(postCssProcessors))
        .pipe(concat('mybundle.css'))
        .pipe(gulp.dest('dest'))
		.pipe(cssmin())
		.pipe(rename({ suffix: '.min' }))
		.pipe(gulp.dest('dest'));
});

gulp.task('dev', function () {
    gulp.watch('src/**/*.scss', ['css']);
});

gulp.task('js', function () {
    gulp.src('src/**/*.es6.js', { read: false })
        .pipe(browserify({
            transform: ['babelify']
        }))
        .pipe(concat('mybundle.js'))
        .pipe(gulp.dest('dest'))
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('dest'));
});