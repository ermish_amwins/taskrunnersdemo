## What is it?

This is a basic demo of Gulp vs. Grunt from a Lunch n Learn session! Feel free to make changes and play around with it!
If you have any questions, just ask Philip Ermish.

The powerpoint from the Lunch n Learn is also available in the [source](https://bitbucket.org/ermish_amwins/taskrunnersdemo/src/2b0e627b04f3e7484d9f223c5325dcbb0dbb820c/Task%20Runners.pptx?fileviewer=file-view-default)