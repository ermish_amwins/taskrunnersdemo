﻿/// <binding BeforeBuild='css' />

module.exports = function (grunt) {
    'use strict';

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        browserify: {
            dist: {
                files: {
                    'dest/mybundle.js': ['src/*.es6.js']
                },
                options: {
                    transform: [
                       ["babelify"]
                    ]
                }
            }
        },
        concat: {
            css: {
                src: 'src/*.css',
                dest: 'dest/mybundle.css'
            }
        },
        cssmin: {
            options: {
                sourcemap: true
            },
            dist: {
                files: [
                  {
                      expand: true, // Recursive
                      cwd: "dest", // The startup directory
                      src: ["**/*.css"], // Source files
                      dest: "dest", // Destination
                      ext: ".min.css" // File extension 
                  }
                ]
            }
        },
        postcss: {
            options: {
                processors: [
                  require('autoprefixer')({ browsers: 'last 20 versions' }) // add vendor prefixes
                ]
            },
            dist: {
                files: [
                  {
                      expand: true, // Recursive
                      cwd: "dest", // The startup directory
                      src: ["**/*.css"], // Source files
                      dest: "dest", // Destination
                      ext: ".css" // File extension 
                  }
                ]
            }
        },
        sass: {
            dist: {
                files: [
                  {
                      expand: true, // Recursive
                      cwd: "src", // The startup directory
                      src: ["**/*.scss"], // Source files
                      dest: "src", // Destination
                      ext: ".css" // File extension 
                  }
                ]
            }
        },
        uglify: {
            js: {
                files: {
                    'dest/bundle.min.js': ['dest/*.js']
                }
            }
        },
        watch: {
            js: {
                files: ["src/**/*.es6.js"],
                tasks: ['newer:browserify']
            },
            css: {
                files: ["src/**/*.scss"],
                tasks: ['newer:sass', 'newer:concat:css', 'newer:postcss', 'newer:cssmin']
            }
        }
    });

    require('load-grunt-tasks')(grunt);

    grunt.registerTask('css', ['sass', 'concat:css', 'postcss', 'cssmin']);
    grunt.registerTask('js', ['browserify', 'uglify']);
    grunt.registerTask('dev', ['watch:css']);
};