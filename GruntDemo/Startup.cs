﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GruntDemo.Startup))]
namespace GruntDemo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
